import requests

VALIDATE_API_URL = "https://api.impresso.com/ops/resetpassword/{}/"
CHANGE_API_URL = "https://api.impresso.com/ops/resetpassword/confirmation/"


def validate_reset_code(reset_code):
    response = requests.get(VALIDATE_API_URL.format(reset_code), headers={'AVCR-Token': 'avcrankararocks'})

    return response.status_code == 200


def change_password(reset_code, password):
    response = requests.post(CHANGE_API_URL, headers={'AVCR-Token': 'avcrankararocks'}, data={'reset_code': reset_code,
                                                                                              'newpassword': password})

    return response.status_code == 200
