from flask_sslify import SSLify
from flask import Flask, request, redirect, render_template

from helpers import validate_reset_code, change_password


application = Flask(__name__)
application.debug = False
sslify = SSLify(application, subdomains=True)


@application.route("/")
@application.route("/index/")
def index():
    return application.send_static_file('index.html')


@application.route("/terms/")
def terms():
    return application.send_static_file('terms.html')


@application.route("/faq/")
def faq():
    return application.send_static_file('faq.html')


@application.route("/resetpassword/<reset_code>", methods=['GET', 'POST'])
def reset_password_view(reset_code):
    if request.method == 'POST':
        if change_password(reset_code, request.form['newpassword']):
            return render_template('confirmation.html', success=True)
        else:
            return render_template('confirmation.html')
    else:
        # Show the password reset form
        if validate_reset_code(reset_code):
            return render_template('reset_password.html', reset_code=reset_code)
        else:
            return render_template('confirmation.html', invalid=True)


@application.errorhandler(404)
def http_error_handler(request):
    return redirect("http://localhost:5000/")


if __name__ == '__main__':
    application.run()
